package tests

import (
	"log"
	"testing"
	"time"
)

func TestUpdateUserAccountEmail(t *testing.T) {
	err := testRepo.UserQuery().UserAccount().UpdateUserAccountEmail(testIdAccountOne, "newflmarsil@student.42.fr")
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user account email has been updated")
}

func TestUpdateUserAccountPseudo(t *testing.T) {
	err := testRepo.UserQuery().UserAccount().UpdateUserAccountPseudo(testIdAccountOne, "newpseudo")
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user account pseudo has been updated")
}

func TestUpdateUserAccountHashedPassword(t *testing.T) {
	newPwd := "383d0fd97170262d5d03de39bb1205ae8a716be28422063378bd59cff3525e01"
	err := testRepo.UserQuery().UserAccount().UpdateUserAccountHashedPassword(testIdAccountOne, newPwd)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user account password has been updated")
}

func TestUpdateUserAccountBirthday(t *testing.T) {
	date := time.Now()
	err := testRepo.UserQuery().UserAccount().UpdateUserAccountBirthday(testIdAccountOne, date)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user account birthday has been updated")
}

func TestUpdateUserAccountLocation(t *testing.T) {
	err := testRepo.UserQuery().UserAccount().UpdateUserAccountLocation(testIdAccountOne, "NewLocation")
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user account location has been updated")
}

func TestUpdateUserAccountPopularity(t *testing.T) {
	err := testRepo.UserQuery().UserAccount().UpdateUserAccountPopularity(testIdAccountOne, 3)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user account popularity has been updated")
}

func TestUpdateUserAccountConfirmationCode(t *testing.T) {
	err := testRepo.UserQuery().UserAccount().UpdateUserAccountConfirmationCode(testIdAccountOne, "00000")
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user account confirmation code has been updated")
}

func TestUpdateUserAccountLastConnection(t *testing.T) {
	err := testRepo.UserQuery().UserAccount().UpdateUserAccountLastConnection(testIdAccountOne, time.Now())
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user account last connection has been updated")
}

func TestUpdateUserAccountVerifiedStatus(t *testing.T) {
	err := testRepo.UserQuery().UserAccount().UpdateUserAccountVerifiedStatus(testIdAccountOne, true)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user account verified status has been updated")
}
