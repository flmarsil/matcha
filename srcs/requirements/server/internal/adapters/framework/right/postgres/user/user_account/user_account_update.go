package user_account

import (
	"fmt"
	"time"
)

func (u *UserAccount) UpdateUserAccountEmail(user_id uint64, email string) error {
	query := "UPDATE t_users_accounts SET user_account_email = $1 WHERE id_user_account = $2;"

	_, err := DB.Exec(query, email, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user account email : %v", err)
	}

	return nil
}

func (u *UserAccount) UpdateUserAccountPseudo(user_id uint64, pseudo string) error {
	query := "UPDATE t_users_accounts SET user_account_pseudo = $1 WHERE id_user_account = $2;"

	_, err := DB.Exec(query, pseudo, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user account pseudo : %v", err)
	}

	return nil
}

func (u *UserAccount) UpdateUserAccountHashedPassword(user_id uint64, hash string) error {
	query := "UPDATE t_users_accounts SET user_account_hash_password = $1 WHERE id_user_account = $2;"

	_, err := DB.Exec(query, hash, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user account password : %v", err)
	}

	return nil
}

func (u *UserAccount) UpdateUserAccountBirthday(user_id uint64, birth time.Time) error {
	query := "UPDATE t_users_accounts SET user_account_birthday = $1 WHERE id_user_account = $2;"

	_, err := DB.Exec(query, birth, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user account birthday : %v", err)
	}

	return nil
}

func (u *UserAccount) UpdateUserAccountLocation(user_id uint64, location string) error {
	query := "UPDATE t_users_accounts SET user_account_location = $1 WHERE id_user_account = $2;"

	_, err := DB.Exec(query, location, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user account location : %v", err)
	}

	return nil
}

func (u *UserAccount) UpdateUserAccountPopularity(user_id uint64, score uint8) error {
	query := "UPDATE t_users_accounts SET user_account_popularity = $1 WHERE id_user_account = $2;"

	_, err := DB.Exec(query, score, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user account popularity score : %v", err)
	}

	return nil
}

func (u *UserAccount) UpdateUserAccountConfirmationCode(user_id uint64, code string) error {
	query := "UPDATE t_users_accounts SET user_account_confirmation_code = $1 WHERE id_user_account = $2;"

	_, err := DB.Exec(query, code, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user account confirmation code : %v", err)
	}

	return nil
}

func (u *UserAccount) UpdateUserAccountLastConnection(user_id uint64, date time.Time) error {
	query := "UPDATE t_users_accounts SET user_account_last_connection_at = $1 WHERE id_user_account = $2;"

	_, err := DB.Exec(query, date, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user account last connection : %v", err)
	}

	return nil
}

func (u *UserAccount) UpdateUserAccountVerifiedStatus(user_id uint64, status bool) error {
	query := "UPDATE t_users_accounts SET user_account_verified_status = $1 WHERE id_user_account = $2;"

	_, err := DB.Exec(query, status, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user account status : %v", err)
	}

	return nil
}
