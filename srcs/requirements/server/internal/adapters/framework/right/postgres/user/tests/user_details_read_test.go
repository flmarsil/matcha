package tests

import (
	"log"
	"testing"
)

func TestGetUserGender(t *testing.T) {
	res, err := testRepo.UserQuery().UserDetails().GetUserGender(testIdAccountOne)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserGender : %v\n", *res)
}

func TestGetUserBio(t *testing.T) {
	res, err := testRepo.UserQuery().UserDetails().GetUserBio(testIdAccountOne)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserBio : %v\n", *res)
}

func TestGetUserInterestInRelationship(t *testing.T) {
	res, err := testRepo.UserQuery().UserDetails().GetUserInterestInRelationship(testIdAccountOne)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserInterestInRelationship : %v\n", *res)
}

func TestGetUserInterestInGender(t *testing.T) {
	res, err := testRepo.UserQuery().UserDetails().GetUserInterestInGender(testIdAccountOne)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserInterestInGender : %v\n", *res)
}

func TestGetUserSizes(t *testing.T) {
	res, err := testRepo.UserQuery().UserDetails().GetUserSize(testIdAccountOne)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserSize : %v\n", *res)
}

func TestGetUserAstrosign(t *testing.T) {
	res, err := testRepo.UserQuery().UserDetails().GetUserAstrosign(testIdAccountOne)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserAstrosign : %v\n", *res)
}

func TestGetUserOrigin(t *testing.T) {
	res, err := testRepo.UserQuery().UserDetails().GetUserOrigin(testIdAccountOne)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserOrigin : %v\n", *res)
}

func TestGetUserHairCut(t *testing.T) {
	res, err := testRepo.UserQuery().UserDetails().GetUserHairCut(testIdAccountOne)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserHairCut : %v\n", *res)
}

func TestGetUserHairColor(t *testing.T) {
	res, err := testRepo.UserQuery().UserDetails().GetUserHairColor(testIdAccountOne)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserHairColor : %v\n", *res)
}

func TestGetUserHairStyle(t *testing.T) {
	res, err := testRepo.UserQuery().UserDetails().GetUserHairStyle(testIdAccountOne)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserHairStyle : %v\n", *res)
}

func TestGetUserEyesColor(t *testing.T) {
	res, err := testRepo.UserQuery().UserDetails().GetUserEyesColor(testIdAccountOne)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserEyesColor : %v\n", *res)
}

func TestGetUserShape(t *testing.T) {
	res, err := testRepo.UserQuery().UserDetails().GetUserShape(testIdAccountOne)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserShapes : %v\n", *res)
}

func TestGetUserStatusConnection(t *testing.T) {
	res, err := testRepo.UserQuery().UserDetails().GetUserStatusConnection(testIdAccountOne)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserStatusConnection : %v\n", res)
}
