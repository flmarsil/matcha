package user_account

import (
	"database/sql"
	"matcha/srcs/requirements/server/internal/domain/model"
	"time"
)

type UserAccountQuery interface {
	CreateUserAccount(user_account model.UserAccount) (uint64, error)
	CreateUserTablesDetails(user_id uint64) error

	GetUserAccountPrivate(user_id uint64) (*model.UserAccount, error)
	GetUserAccountPublic(user_id uint64) (*model.UserAccount, error)
	GetUserAccountIdByPseudo(pseudo string) (uint64, error)
	GetUserAccountIdByEmail(email string) (uint64, error)
	// GetUserAccountPseudoById(user_id uint64) (*string, error)
	GetUserAccountHashedPasswordByEmail(email string) (*string, error)
	GetUserAccountHashedPasswordById(user_id uint64) (*string, error)
	GetUserAccountConfirmationCode(user_id uint64) (*string, error)

	UpdateUserAccountEmail(user_id uint64, email string) error
	UpdateUserAccountPseudo(user_id uint64, pseudo string) error
	UpdateUserAccountHashedPassword(user_id uint64, hash string) error
	UpdateUserAccountBirthday(user_id uint64, birth time.Time) error
	UpdateUserAccountLocation(user_id uint64, location string) error
	UpdateUserAccountPopularity(user_id uint64, score uint8) error
	UpdateUserAccountConfirmationCode(user_id uint64, code string) error
	UpdateUserAccountLastConnection(user_id uint64, date time.Time) error
	UpdateUserAccountVerifiedStatus(user_id uint64, status bool) error
}

var DB *sql.DB

type UserAccount struct{}
