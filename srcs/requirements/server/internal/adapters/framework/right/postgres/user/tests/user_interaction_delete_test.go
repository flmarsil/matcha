package tests

import (
	"log"
	"testing"
)

func TestDeleteUserLike(t *testing.T) {
	err := testRepo.UserQuery().UserInteraction().DeleteUserLike(testIdAccountOne, testIdAccountTwo)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Println("DeleteUserLike : like has been deleted")
}

func TestDeleteUserBlock(t *testing.T) {
	err := testRepo.UserQuery().UserInteraction().DeleteUserBlock(testIdAccountOne, testIdAccountTwo)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Println("DeleteUserBlock : block has been deleted")
}
