package postgres

import (
	"database/sql"
	"log"
	u "matcha/srcs/requirements/server/internal/adapters/framework/right/postgres/user"

	_ "github.com/lib/pq"
)

type Repository interface {
	UserQuery() u.UserQueryRepository
}

type repository struct{}

var DB *sql.DB

func NewRepository(db *sql.DB) Repository {
	DB = db
	return &repository{}
}

func NewDB() (*sql.DB, error) {
	DB, err := sql.Open("postgres", "POSTGRES_URL")
	if err != nil {
		log.Fatalf("Db connetion failure : %v\n", err)
	}

	// err = DB.Ping()
	// if err != nil {
	// 	log.Fatalf("Db ping failure : %v\n", err)
	// }

	return DB, nil
}

func CloseDB() {
	err := DB.Close()
	if err != nil {
		log.Fatalf("Db close failure : %v\n", err)
	}
}

func (r *repository) UserQuery() u.UserQueryRepository {
	u.DB = DB
	return &u.UserQuery{}
}
