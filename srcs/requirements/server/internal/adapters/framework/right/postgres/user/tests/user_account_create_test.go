package tests

import (
	"log"
	"testing"
)

func TestCreateUserAccount(t *testing.T) {
	testIdAccountOne, err := testRepo.UserQuery().UserAccount().CreateUserAccount(testAccountOne)

	if err != nil {
		log.Fatalf("create user error : %v\n", err)
	}

	log.Printf("user has been created, id : %v\n", testIdAccountOne)

	testIdAccountTwo, err := testRepo.UserQuery().UserAccount().CreateUserAccount(testAccountOne)

	if err != nil {
		log.Fatalf("create user error : %v\n", err)
	}

	log.Printf("user has been created, id : %v\n", testIdAccountTwo)
}

func TestCreateUserTablesDetails(t *testing.T) {
	err := testRepo.UserQuery().UserAccount().CreateUserTablesDetails(testIdAccountOne) // id testAccountOne
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("tables user has been initialized")

	err = testRepo.UserQuery().UserAccount().CreateUserTablesDetails(testIdAccountTwo) // id testAccountTwo
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
}
