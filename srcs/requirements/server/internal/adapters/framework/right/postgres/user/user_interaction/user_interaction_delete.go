package user_interaction

import (
	"fmt"
)

func (u *UserInteraction) DeleteUserLike(user_id_given, user_id_received uint64) error {
	query := "DELETE FROM t_users_likes WHERE user_account_id_given = $1 AND user_account_id_received = $2;"

	_, err := DB.Exec(query, user_id_given, user_id_received)
	if err != nil {
		return fmt.Errorf("cannot delete user like : %v", err)
	}
	return nil
}

func (u *UserInteraction) DeleteUserBlock(user_id_given, user_id_received uint64) error {
	query := "DELETE FROM t_users_blocks WHERE user_account_id_given = $1 AND user_account_id_received = $2;"

	_, err := DB.Exec(query, user_id_given, user_id_received)
	if err != nil {
		return fmt.Errorf("cannot delete user like : %v", err)
	}
	return nil
}
