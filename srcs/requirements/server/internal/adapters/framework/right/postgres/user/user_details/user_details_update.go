package user_details

import (
	"fmt"
)

func (u *UserDetails) UpdateUserBio(user_id uint64, bio string) error {
	query := "UPDATE t_users_bios SET user_bio_content = $1 WHERE user_account_id = $2;"

	_, err := DB.Exec(query, bio, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user biography : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserInterestInRelationship(user_id uint64, interest string) error {
	query := "UPDATE t_users_interest_in_relationships SET relationship_id = (SELECT id_relationship FROM t_relationships WHERE relationship_content = $1) WHERE user_account_id = $2;"

	_, err := DB.Exec(query, interest, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user interest in relationship : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserInterestInGender(user_id uint64, gender string) error {
	query := "UPDATE t_users_interest_in_genders SET gender_id = (SELECT id_gender FROM t_genders WHERE gender_content = $1) WHERE user_account_id = $2;"

	_, err := DB.Exec(query, gender, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user interest in gender : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserAstrosign(user_id uint64, astrosign string) error {
	query := "UPDATE t_users_astrosigns SET astrosign_id = (SELECT id_astrosign FROM t_astrosigns WHERE astrosign_content = $1) WHERE user_account_id = $2;"

	_, err := DB.Exec(query, astrosign, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user astrosign : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserOrigin(user_id uint64, origin string) error {
	query := "UPDATE t_users_origins SET origin_id = (SELECT id_origin FROM t_origins WHERE origin_content = $1) WHERE user_account_id = $2;"

	_, err := DB.Exec(query, origin, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user origin : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserHairCut(user_id uint64, hair_cut string) error {
	query := "UPDATE t_users_hairs_cuts SET hair_cut_id = (SELECT id_hair_cut FROM t_hairs_cuts WHERE hair_cut_content = $1) WHERE user_account_id = $2;"

	_, err := DB.Exec(query, hair_cut, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user hair cut : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserHairColor(user_id uint64, hair_color string) error {
	query := "UPDATE t_users_hairs_colors SET hair_color_id = (SELECT id_hair_color FROM t_hairs_colors WHERE hair_color_content = $1) WHERE user_account_id = $2;"

	_, err := DB.Exec(query, hair_color, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user hair color : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserHairStyle(user_id uint64, hair_style string) error {
	query := "UPDATE t_users_hairs_styles SET hair_style_id = (SELECT id_hair_style FROM t_hairs_styles WHERE hair_style_content = $1) WHERE user_account_id = $2;"

	_, err := DB.Exec(query, hair_style, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user hair style : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserEyesColor(user_id uint64, eye_color string) error {
	query := "UPDATE t_users_eyes SET eye_id = (SELECT id_eye FROM t_eyes WHERE eye_content = $1) WHERE user_account_id = $2;"

	_, err := DB.Exec(query, eye_color, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user eyes color : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserShape(user_id uint64, shape string) error {
	query := "UPDATE t_users_shapes SET shape_id = (SELECT id_shape FROM t_shapes WHERE shape_content = $1) WHERE user_account_id = $2;"

	_, err := DB.Exec(query, shape, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user shapes : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserSize(user_id uint64, size string) error {
	query := "UPDATE t_users_sizes SET size_id = (SELECT id_size FROM t_sizes WHERE size_content = $1) WHERE user_account_id = $2;"

	_, err := DB.Exec(query, size, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user sizes : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserGender(user_id uint64, gender string) error {
	query := "UPDATE t_users_genders SET gender_id = (SELECT id_gender FROM t_genders WHERE gender_content = $1) WHERE user_account_id = $2;"

	_, err := DB.Exec(query, gender, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user gender : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserStatusConnection(user_id uint64, status bool) error {
	query := "UPDATE t_users_status_connections SET status_connection = $1 WHERE user_account_id = $2;"

	_, err := DB.Exec(query, status, user_id)
	if err != nil {
		return fmt.Errorf("cannot update user status connection : %v", err)
	}

	return nil
}
