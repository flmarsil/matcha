package user_interaction

import (
	"fmt"
)

func (u *UserInteraction) CreateUserVisit(user_id_given, user_id_received uint64) error {
	query := "INSERT INTO t_users_visits (user_account_id_given, user_account_id_received) VALUES($1, $2);"

	_, err := DB.Exec(query, user_id_given, user_id_received)
	if err != nil {
		return fmt.Errorf("cannot add user visit : %v", err)
	}

	return nil
}

func (u *UserInteraction) CreateUserLike(user_id_given, user_id_received uint64) error {
	query := "INSERT INTO t_users_likes (user_account_id_given, user_account_id_received) VALUES($1, $2);"

	_, err := DB.Exec(query, user_id_given, user_id_received)
	if err != nil {
		return fmt.Errorf("cannot add user like : %v", err)
	}
	return nil
}

func (u *UserInteraction) CreateUserBlock(user_id_given, user_id_received uint64) error {
	query := "INSERT INTO t_users_blocks (user_account_id_given, user_account_id_received) VALUES($1, $2);"

	_, err := DB.Exec(query, user_id_given, user_id_received)
	if err != nil {
		return fmt.Errorf("cannot add user like : %v", err)
	}
	return nil
}

func (u *UserInteraction) CreateUserReport(user_id_given, user_id_received uint64) error {
	query := "INSERT INTO t_users_reports (user_account_id_given, user_account_id_received) VALUES($1, $2);"

	_, err := DB.Exec(query, user_id_given, user_id_received)
	if err != nil {
		return fmt.Errorf("cannot add user like : %v", err)
	}
	return nil
}
