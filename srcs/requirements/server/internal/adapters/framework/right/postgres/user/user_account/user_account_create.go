package user_account

import (
	"fmt"
	"matcha/srcs/requirements/server/internal/domain/model"
)

func (u *UserAccount) CreateUserAccount(user_account model.UserAccount) (uint64, error) {
	query := "INSERT INTO t_users_accounts (user_account_email, user_account_hash_password, user_account_pseudo, user_account_birthday) VALUES($1, $2, $3, $4) RETURNING id_user_account;"

	var id uint64

	err := DB.QueryRow(query, user_account.Email, user_account.Password, user_account.Pseudo, user_account.Birthday).Scan(&id)
	if err != nil {
		return 0, fmt.Errorf("cannot create user in database : %v", err)
	}

	return id, nil
}

/*
	Create all tables about the user's details by initializing them to NULL with his user id.
	Avoid a query to the db to check if a table exists before an update.
	Avoid having to create as many setters functions as updates to create the table at each change of the user details if it does not exist yet.
*/

func (u *UserAccount) CreateUserTablesDetails(user_id uint64) error {
	tables := make(map[int]string)

	tables[0] = "t_users_bios"
	tables[1] = "t_users_photos"
	tables[2] = "t_users_interest_in_relationships"
	tables[3] = "t_users_interest_in_genders"
	tables[4] = "t_users_astrosigns"
	tables[5] = "t_users_origins"
	tables[6] = "t_users_hairs_cuts"
	tables[7] = "t_users_hairs_colors"
	tables[8] = "t_users_hairs_styles"
	tables[9] = "t_users_eyes"
	tables[10] = "t_users_shapes"
	tables[11] = "t_users_sizes"
	tables[12] = "t_users_genders"
	tables[13] = "t_users_status_connections"
	// add new table here if you create other user's details in db

	for _, table := range tables {
		query := "INSERT INTO " + table + " (user_account_id) VALUES ($1);"

		_, err := DB.Exec(query, user_id)
		if err != nil {
			return fmt.Errorf("cannot set %v table : %v", table, err)
		}
	}

	return nil
}
