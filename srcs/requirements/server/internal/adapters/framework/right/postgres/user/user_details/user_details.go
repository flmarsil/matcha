package user_details

import "database/sql"

type UserDetailsQuery interface {
	GetUserGender(user_id uint64) (*string, error)
	GetUserBio(user_id uint64) (*string, error)
	GetUserInterestInRelationship(user_id uint64) (*string, error)
	GetUserInterestInGender(user_id uint64) (*string, error)
	GetUserSize(user_id uint64) (*string, error)
	GetUserAstrosign(user_id uint64) (*string, error)
	GetUserOrigin(user_id uint64) (*string, error)
	GetUserHairCut(user_id uint64) (*string, error)
	GetUserHairColor(user_id uint64) (*string, error)
	GetUserHairStyle(user_id uint64) (*string, error)
	GetUserEyesColor(user_id uint64) (*string, error)
	GetUserShape(user_id uint64) (*string, error)
	GetUserStatusConnection(user_id uint64) (bool, error)
	// GetUserPhotos(user_id uint64) ([]string, error)
	// GetUserVisits()
	// GetUserLikes()

	UpdateUserGender(user_id uint64, gender string) error
	UpdateUserBio(user_id uint64, bio string) error
	UpdateUserInterestInRelationship(user_id uint64, interest string) error
	UpdateUserInterestInGender(user_id uint64, interest string) error
	UpdateUserSize(user_id uint64, size string) error
	UpdateUserAstrosign(user_id uint64, astrosign string) error
	UpdateUserOrigin(user_id uint64, origin string) error
	UpdateUserHairCut(user_id uint64, hair_cut string) error
	UpdateUserHairColor(user_id uint64, hair_color string) error
	UpdateUserHairStyle(user_id uint64, hair_style string) error
	UpdateUserEyesColor(user_id uint64, eye_color string) error
	UpdateUserShape(user_id uint64, shape string) error
	UpdateUserStatusConnection(user_id uint64, status bool) error
	// UpdateUserPhotos(user_id uint64, content []byte) error
}

var DB *sql.DB

type UserDetails struct{}
