package user_account

import (
	"matcha/srcs/requirements/server/internal/domain/model"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (u *UserAccount) GetUserAccountPrivate(user_id uint64) (*model.UserAccount, error) {
	query := "SELECT user_account_email, user_account_pseudo, user_account_birthday, user_account_location FROM t_users_accounts WHERE id_user_account = $1;"

	var account model.UserAccount

	err := DB.QueryRow(query, user_id).Scan(&account.Email, &account.Pseudo, &account.Birthday, &account.Location)
	if err != nil {
		return nil, status.Errorf(codes.NotFound, "cannot get private user account : %v\n", err)
	}

	return &account, nil
}

func (u *UserAccount) GetUserAccountPublic(user_id uint64) (*model.UserAccount, error) {
	query := "SELECT user_account_pseudo, user_account_birthday, user_account_location, user_account_popularity, user_account_last_connection_at, user_account_verified_status FROM t_users_accounts WHERE id_user_account = $1;"

	var account model.UserAccount

	err := DB.QueryRow(query, user_id).Scan(&account.Pseudo, &account.Birthday, &account.Location, &account.Popularity, &account.LastConnection, &account.StatusVerified)
	if err != nil {
		return nil, status.Errorf(codes.NotFound, "cannot get public user account : %v\n", err)
	}

	return &account, nil
}

func (u *UserAccount) GetUserAccountIdByPseudo(pseudo string) (uint64, error) {
	query := "SELECT id_user_account FROM t_users_accounts WHERE user_account_pseudo = $1;"

	id := 0

	err := DB.QueryRow(query, pseudo).Scan(&id)
	if err != nil {
		return 0, status.Errorf(codes.NotFound, "cannot get user id by pseudo : %v\n", err)
	}

	return uint64(id), nil
}

func (u *UserAccount) GetUserAccountIdByEmail(email string) (uint64, error) {
	query := "SELECT id_user_account FROM t_users_accounts WHERE user_account_email = $1;"

	id := 0

	err := DB.QueryRow(query, email).Scan(&id)
	if err != nil {
		return 0, status.Errorf(codes.NotFound, "cannot get user id by email : %v\n", err)
	}

	return uint64(id), nil
}

func (u *UserAccount) GetUserAccountHashedPasswordByEmail(email string) (*string, error) {
	query := "SELECT user_account_hash_password FROM t_users_accounts WHERE user_account_email = $1;"

	var hashedPassword string

	err := DB.QueryRow(query, email).Scan(&hashedPassword)
	if err != nil {
		return nil, status.Errorf(codes.NotFound, "email and password don't match : %v\n", err)
	}

	return &hashedPassword, nil
}

func (u *UserAccount) GetUserAccountHashedPasswordById(user_id uint64) (*string, error) {
	query := "SELECT user_account_hash_password FROM t_users_accounts WHERE id_user_account = $1;"

	var hashedPassword string

	err := DB.QueryRow(query, user_id).Scan(&hashedPassword)
	if err != nil {
		return nil, status.Errorf(codes.NotFound, "id and password don't match : %v\n", err)
	}

	return &hashedPassword, nil
}

func (u *UserAccount) GetUserAccountConfirmationCode(user_id uint64) (*string, error) {
	query := "SELECT user_account_confirmation_code FROM t_users_accounts WHERE id_user_account = $1;"

	code := ""

	DB.QueryRow(query, user_id).Scan(&code)

	return &code, nil
}
