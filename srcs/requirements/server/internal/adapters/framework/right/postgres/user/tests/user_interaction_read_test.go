package tests

import (
	"log"
	"testing"
)

func TestGetIfUserIdExist(t *testing.T) {
	res, err := testRepo.UserQuery().UserInteraction().GetIfUserIdExist(testIdAccountOne)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetIfUserIdExist : %v\n", res)

}

func TestGetIfOtherUserLikedUser(t *testing.T) {
	res, err := testRepo.UserQuery().UserInteraction().GetIfOtherUserLikedUser(testIdAccountOne, testIdAccountTwo)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetIfOtherUserLikedUser : %v\n", res)
}

func TestGetIfOtherUserBlockedUser(t *testing.T) {
	res, err := testRepo.UserQuery().UserInteraction().GetIfOtherUserBlockedUser(testIdAccountOne, testIdAccountTwo)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetIfOtherUserLikedUser : %v\n", res)
}
