package tests

import (
	"log"
	"testing"
)

func TestUpdateUserBio(t *testing.T) {
	err := testRepo.UserQuery().UserDetails().UpdateUserBio(testIdAccountOne, "Hello World ! This is my new biography !")
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user biography has been updated")
}

func TestUpdateUserInterestInRelationship(t *testing.T) {
	err := testRepo.UserQuery().UserDetails().UpdateUserInterestInRelationship(testIdAccountOne, "cdi")
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user interest in relationship has been updated")
}

func TestUpdateUserInterestInGender(t *testing.T) {
	err := testRepo.UserQuery().UserDetails().UpdateUserInterestInGender(testIdAccountOne, "femme")
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user interest in gender has been updated")
}

func TestUpdateUserAstrosign(t *testing.T) {
	err := testRepo.UserQuery().UserDetails().UpdateUserAstrosign(testIdAccountOne, "scorpion")
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user astrosign has been updated")
}

func TestUpdateUserOrigin(t *testing.T) {
	err := testRepo.UserQuery().UserDetails().UpdateUserOrigin(testIdAccountOne, "latines")
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user origin has been updated")
}
func TestUpdateUserHairCut(t *testing.T) {
	err := testRepo.UserQuery().UserDetails().UpdateUserHairCut(testIdAccountOne, "courts")
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user hair cut has been updated")
}

func TestUpdateUserHairColor(t *testing.T) {
	err := testRepo.UserQuery().UserDetails().UpdateUserHairColor(testIdAccountOne, "chatain")
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user hair color has been updated")
}

func TestUpdateUserHairStyle(t *testing.T) {
	err := testRepo.UserQuery().UserDetails().UpdateUserHairStyle(testIdAccountOne, "raides")
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user hair cut has been updated")
}

func TestUpdateUserEyesColor(t *testing.T) {
	err := testRepo.UserQuery().UserDetails().UpdateUserEyesColor(testIdAccountOne, "marrons")
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user eye color has been updated")
}

func TestUpdateUserShape(t *testing.T) {
	err := testRepo.UserQuery().UserDetails().UpdateUserShape(testIdAccountOne, "normal")
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user shape has been updated")
}
func TestUpdateUserSize(t *testing.T) {
	err := testRepo.UserQuery().UserDetails().UpdateUserSize(testIdAccountOne, "180")
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user size has been updated")
}
func TestUpdateUserGender(t *testing.T) {
	err := testRepo.UserQuery().UserDetails().UpdateUserGender(testIdAccountOne, "homme")
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user gender has been updated")
}

func TestUpdateUserStatusConnection(t *testing.T) {
	err := testRepo.UserQuery().UserDetails().UpdateUserStatusConnection(testIdAccountOne, true)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user account verified status has been updated")
}
