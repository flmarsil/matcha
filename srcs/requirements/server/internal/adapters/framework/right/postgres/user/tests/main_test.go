package tests

import (
	"database/sql"
	"log"
	"matcha/srcs/requirements/server/internal/adapters/framework/right/postgres"
	"matcha/srcs/requirements/server/internal/domain/model"
	"os"
	"testing"
	"time"
)

const (
	dbName = "postgres"
	dbURL  = "postgres://flmarsil:flmarsil@0.0.0.0:5432/matcha?sslmode=disable"
)

var testRepo postgres.Repository

var testAccountOne = model.UserAccount{
	Email:          "flmarsil@student.42.fr",
	Password:       "c152246c91ef62f553d2109b68698b19f7dd83328374abc489920bf2e2e23510",
	Pseudo:         "flmarsil",
	Birthday:       time.Date(1994, 10, 31, 00, 00, 00, 0, time.UTC),
	Location:       "Paris",
	LastConnection: time.Now(),
	StatusVerified: false,
}

var testAccountTwo = model.UserAccount{
	Email:          "fassani@student.42.fr",
	Password:       "c88506eac4b7eddf0e5c7dcd1eca7389683e3fbcffee62573e4f520ea8e51f77",
	Pseudo:         "fassani",
	Birthday:       time.Date(1997, 5, 11, 00, 00, 00, 0, time.UTC),
	Location:       "Lyon",
	LastConnection: time.Now(),
	StatusVerified: false,
}

var testIdAccountOne = uint64(1)
var testIdAccountTwo = uint64(2)

func TestMain(m *testing.M) {
	database, err := sql.Open(dbName, dbURL)
	if err != nil {
		log.Fatalf("Connection db failure : %v\n", err)
	}

	testRepo = postgres.NewRepository(database)
	os.Exit(m.Run())
}
