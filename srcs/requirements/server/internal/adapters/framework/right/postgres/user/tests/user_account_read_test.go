package tests

import (
	"log"
	"testing"
)

func TestGetUserAccountPrivate(t *testing.T) {
	account, err := testRepo.UserQuery().UserAccount().GetUserAccountPrivate(testIdAccountOne)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserAccountPrivate : %v\n", *account)
}

func TestGetUserAccountPublic(t *testing.T) {
	account, err := testRepo.UserQuery().UserAccount().GetUserAccountPublic(testIdAccountOne)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserAccountPublic : %v\n", *account)
}

func TestGetUserAccountIdByPseudo(t *testing.T) {
	userId, err := testRepo.UserQuery().UserAccount().GetUserAccountIdByPseudo(testAccountOne.Pseudo)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserID : %v\n", userId)
}

func TestGetUserAccountIdByEmail(t *testing.T) {
	userId, err := testRepo.UserQuery().UserAccount().GetUserAccountIdByEmail(testAccountOne.Email)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserIdByEmail : %v\n", userId)
}

func TestGetUserAccountHashedPasswordByEmail(t *testing.T) {
	userPwd, err := testRepo.UserQuery().UserAccount().GetUserAccountHashedPasswordByEmail(testAccountOne.Email)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserAccountHashedPasswordByEmail : %v\n", *userPwd)
}

func TestGetUserAccountHashedPasswordById(t *testing.T) {
	userPwd, err := testRepo.UserQuery().UserAccount().GetUserAccountHashedPasswordById(testIdAccountOne)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserAccountHashedPasswordByEmail : %v\n", *userPwd)
}

func TestGetUserAccountConfirmationCode(t *testing.T) {
	code, err := testRepo.UserQuery().UserAccount().GetUserAccountConfirmationCode(testIdAccountOne)
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("GetUserAccountConfirmationCode : %v\n", *code)
}
