package tests

import (
	"log"
	"testing"
)

func TestCreateUserVisit(t *testing.T) {
	err := testRepo.UserQuery().UserInteraction().CreateUserVisit(testIdAccountOne, testIdAccountTwo) // testAccountOne visit profile of testAccountTwo
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user visit has been added")
}

func TestCreateUserLike(t *testing.T) {
	err := testRepo.UserQuery().UserInteraction().CreateUserLike(testIdAccountOne, testIdAccountTwo) // testAccountOne visit profile of testAccountTwo
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user like has been added")
}
func TestCreateUserBlock(t *testing.T) {
	err := testRepo.UserQuery().UserInteraction().CreateUserBlock(testIdAccountOne, testIdAccountTwo) // testAccountOne visit profile of testAccountTwo
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user block has been added")

}
func TestCreateUserReport(t *testing.T) {
	err := testRepo.UserQuery().UserInteraction().CreateUserReport(testIdAccountOne, testIdAccountTwo) // testAccountOne visit profile of testAccountTwo
	if err != nil {
		log.Fatalf("Error : %v\n", err)
	}
	log.Printf("user report has been added")
}
