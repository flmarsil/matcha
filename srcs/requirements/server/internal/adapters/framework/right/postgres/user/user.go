package user

import (
	"database/sql"
	uaq "matcha/srcs/requirements/server/internal/adapters/framework/right/postgres/user/user_account"
	udq "matcha/srcs/requirements/server/internal/adapters/framework/right/postgres/user/user_details"
	uiq "matcha/srcs/requirements/server/internal/adapters/framework/right/postgres/user/user_interaction"
)

type UserQueryRepository interface {
	UserAccount() uaq.UserAccountQuery
	UserDetails() udq.UserDetailsQuery
	UserInteraction() uiq.UserInteractionQuery
}

type UserQuery struct{}

var DB *sql.DB

func (u *UserQuery) UserAccount() uaq.UserAccountQuery {
	uaq.DB = DB
	return &uaq.UserAccount{}
}

func (u *UserQuery) UserDetails() udq.UserDetailsQuery {
	udq.DB = DB
	return &udq.UserDetails{}
}

func (u *UserQuery) UserInteraction() uiq.UserInteractionQuery {
	uiq.DB = DB
	return &uiq.UserInteraction{}
}
