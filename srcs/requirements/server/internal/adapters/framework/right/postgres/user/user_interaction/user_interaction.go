package user_interaction

import "database/sql"

type UserInteractionQuery interface {
	CreateUserVisit(user_id_given, user_id_received uint64) error
	CreateUserLike(user_id_given, user_id_received uint64) error
	CreateUserBlock(user_id_given, user_id_received uint64) error
	CreateUserReport(user_id_given, user_id_received uint64) error

	GetIfUserIdExist(user_id uint64) (bool, error)
	GetIfOtherUserLikedUser(user_id, other_id uint64) (bool, error)
	GetIfOtherUserBlockedUser(user_id, other_id uint64) (bool, error)

	DeleteUserLike(user_id_given, user_id_received uint64) error
	DeleteUserBlock(user_id_given, user_id_received uint64) error
}

var DB *sql.DB

type UserInteraction struct{}
