package user_details

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (u *UserDetails) GetUserGender(user_id uint64) (*string, error) {
	query := "SELECT gender_content FROM t_users_genders INNER JOIN t_genders ON t_users_genders.gender_id = t_genders.id_gender WHERE user_account_id = $1;"

	var userGender string

	err := DB.QueryRow(query, user_id).Scan(&userGender)
	if err != nil {
		return nil, status.Errorf(codes.NotFound, "cannot get user gender : %v\n", err)
	}

	return &userGender, nil
}

func (u *UserDetails) GetUserBio(user_id uint64) (*string, error) {
	query := "SELECT user_bio_content FROM t_users_bios WHERE user_account_id = $1;"

	userBio := ""

	DB.QueryRow(query, user_id).Scan(&userBio)

	return &userBio, nil
}

func (u *UserDetails) GetUserInterestInRelationship(user_id uint64) (*string, error) {
	query := "SELECT relationship_content FROM t_users_interest_in_relationships INNER JOIN t_relationships ON t_users_interest_in_relationships.relationship_id = t_relationships.id_relationship WHERE user_account_id = $1;"

	interest := ""

	DB.QueryRow(query, user_id).Scan(&interest)

	return &interest, nil
}

func (u *UserDetails) GetUserSize(user_id uint64) (*string, error) {
	query := "SELECT size_content FROM t_users_sizes INNER JOIN t_sizes ON t_users_sizes.size_id = t_sizes.id_size WHERE user_account_id = $1;"

	size := ""

	DB.QueryRow(query, user_id).Scan(&size)

	return &size, nil
}

func (u *UserDetails) GetUserInterestInGender(user_id uint64) (*string, error) {
	query := "SELECT gender_content FROM t_users_interest_in_genders INNER JOIN t_genders ON t_users_interest_in_genders.gender_id = t_genders.id_gender WHERE user_account_id = $1;"

	interest := ""

	DB.QueryRow(query, user_id).Scan(&interest)

	return &interest, nil
}

func (u *UserDetails) GetUserAstrosign(user_id uint64) (*string, error) {
	query := "SELECT astrosign_content FROM t_users_astrosigns INNER JOIN t_astrosigns ON t_users_astrosigns.astrosign_id = t_astrosigns.id_astrosign WHERE user_account_id = $1;"

	astrosign := ""

	DB.QueryRow(query, user_id).Scan(&astrosign)

	return &astrosign, nil
}

func (u *UserDetails) GetUserOrigin(user_id uint64) (*string, error) {
	query := "SELECT origin_content FROM t_users_origins INNER JOIN t_origins ON t_users_origins.origin_id = t_origins.id_origin WHERE user_account_id = $1;"

	origin := ""

	DB.QueryRow(query, user_id).Scan(&origin)

	return &origin, nil
}

func (u *UserDetails) GetUserHairCut(user_id uint64) (*string, error) {
	query := "SELECT hair_cut_content FROM t_users_hairs_cuts INNER JOIN t_hairs_cuts ON t_users_hairs_cuts.hair_cut_id = t_hairs_cuts.id_hair_cut WHERE user_account_id = $1;"

	hairCut := ""

	DB.QueryRow(query, user_id).Scan(&hairCut)

	return &hairCut, nil
}

func (u *UserDetails) GetUserHairStyle(user_id uint64) (*string, error) {
	query := "SELECT hair_style_content FROM t_users_hairs_styles INNER JOIN t_hairs_styles ON t_users_hairs_styles.hair_style_id = t_hairs_styles.id_hair_style WHERE user_account_id = $1;"

	hairStyle := ""

	DB.QueryRow(query, user_id).Scan(&hairStyle)

	return &hairStyle, nil
}

func (u *UserDetails) GetUserHairColor(user_id uint64) (*string, error) {
	query := "SELECT hair_color_content FROM t_users_hairs_colors INNER JOIN t_hairs_colors ON t_users_hairs_colors.hair_color_id = t_hairs_colors.id_hair_color WHERE user_account_id = $1;"

	hairColor := ""

	DB.QueryRow(query, user_id).Scan(&hairColor)

	return &hairColor, nil
}

func (u *UserDetails) GetUserEyesColor(user_id uint64) (*string, error) {
	query := "SELECT eye_content FROM t_users_eyes INNER JOIN t_eyes ON t_users_eyes.eye_id = t_eyes.id_eye WHERE user_account_id = $1;"

	eyeColor := ""

	DB.QueryRow(query, user_id).Scan(&eyeColor)

	return &eyeColor, nil
}

func (u *UserDetails) GetUserShape(user_id uint64) (*string, error) {
	query := "SELECT shape_content FROM t_users_shapes INNER JOIN t_shapes ON t_users_shapes.shape_id = t_shapes.id_shape WHERE user_account_id = $1;"

	shape := ""

	DB.QueryRow(query, user_id).Scan(&shape)

	return &shape, nil
}

func (u *UserDetails) GetUserStatusConnection(user_id uint64) (bool, error) {
	query := "SELECT status_connection FROM t_users_status_connections WHERE user_account_id = $1;"

	connection := false

	DB.QueryRow(query, user_id).Scan(&connection)

	return connection, nil
}
