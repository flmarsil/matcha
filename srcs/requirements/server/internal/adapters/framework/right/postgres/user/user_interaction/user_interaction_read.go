package user_interaction

import (
	"fmt"
)

func (u *UserInteraction) GetIfUserIdExist(user_id uint64) (bool, error) {
	query := "SELECT EXISTS (SELECT 1 FROM t_users_accounts WHERE id_user_account = $1);"

	var response bool

	err := DB.QueryRow(query, user_id).Scan(&response)
	if err != nil {
		return false, fmt.Errorf("cannot check if id already exist : %v", err)
	}

	if !response {
		return false, nil
	}

	return true, nil
}

func (u *UserInteraction) GetIfOtherUserLikedUser(user_id, other_id uint64) (bool, error) {
	query := "SELECT EXISTS (SELECT 1 FROM t_users_likes WHERE user_account_id_given = $1 AND user_account_id_received = $2);"

	var response bool

	err := DB.QueryRow(query, other_id, user_id).Scan(&response)
	if err != nil {
		return false, fmt.Errorf("cannot check if other user liked user : %v", err)
	}

	if !response {
		return false, nil
	}

	return true, nil
}

func (u *UserInteraction) GetIfOtherUserBlockedUser(user_id, other_id uint64) (bool, error) {
	query := "SELECT EXISTS (SELECT 1 FROM t_users_blocks WHERE user_account_id_given = $1 AND user_account_id_received = $2);"

	var response bool

	err := DB.QueryRow(query, other_id, user_id).Scan(&response)
	if err != nil {
		return false, fmt.Errorf("cannot check if other user blocked user : %v", err)
	}

	if !response {
		return false, nil
	}

	return true, nil
}
