package model

import (
	"time"
)

type UserAccount struct {
	Id               uint64    `db:"id_user_account"`
	Email            string    `db:"user_account_email"`
	Password         string    `db:"user_account_hash_password"`
	Pseudo           string    `db:"user_account_pseudo"`
	Birthday         time.Time `db:"user_account_birthday"`
	Location         string    `db:"user_account_location"`
	Popularity       uint8     `db:"user_account_popularity"`
	CodeConfirmation uint8     `db:"user_account_confirmation_code"`
	CreatedAt        time.Time `db:"user_account_created_at"`
	LastConnection   time.Time `db:"user_account_last_connection_at"`
	StatusVerified   bool      `db:"user_account_verified_status"`
}

type UserDetails struct {
	Pseudo                 string    `db:"user_account_pseudo"`
	Location               string    `db:"user_account_location"`
	Birthday               time.Time `db:"user_account_birthday"`
	Popularity             uint8     `db:"user_account_popularity"`
	LastConnection         time.Time `db:"user_account_last_connection_at"`
	StatusConnection       bool      `db:"status_connection"`
	Bio                    string    `db:"user_bio_content"`
	Photos                 []string  `db:"user_photo_link"`
	InterestInRelationship string    `db:"relationship_content"`
	InterestInGender       string    `db:"gender_content"`
	Astrosign              string    `db:"astrosign_content"`
	Origins                string    `db:"origin_content"`
	HairCut                string    `db:"hair_cut_content"`
	HairColor              string    `db:"hair_color_content"`
	HairStyle              string    `db:"hair_style_content"`
	EyesColor              string    `db:"eye_content"`
	Shape                  string    `db:"shape_content"`
	Size                   string    `db:"size_content"`
	LikedVisitor           bool      `db:"user_account_id_given"`
}
