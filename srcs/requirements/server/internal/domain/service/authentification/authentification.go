package authentification

import "matcha/srcs/requirements/server/internal/adapters/framework/right/postgres"

type Auth interface {
	SignUp()
	LogIn()
	LogOut()
	RecoveryPassword()
}

type auth struct {
	repo postgres.Repository
	// tokenManager TokenManager
}

func (a *auth) SignUp() {
	// email, hash pwd, pseudo, birthday
	// create all details tables in db with id return
	// update popularity to 5/5
	// update verified status to false
	// send email with confirmation code

}

func (a *auth) LogIn() {

}

func (a *auth) LogOut() {

}

func (a *auth) RecoveryPassword() {

}
