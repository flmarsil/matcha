package tests

package tests

// import (
// 	"log"
// 	"testing"
// )

// func TestServiceGetUserDetails(t *testing.T) {
// 	userDetails, err := testServiceUser.GetUserDetails(testAccountOne.Pseudo, testIdAccountOne)
// 	if err != nil {
// 		log.Fatalf("Error : %v\n", err)
// 	}

// 	log.Println(userDetails)
// }

// func TestServiceGetUserAccount(t *testing.T) {
// 	userAccount, err := testServiceUser.GetUserAccount(testIdAccountOne, testIdAccountOne)
// 	if err != nil {
// 		log.Fatalf("Error : %v\n", err)
// 	}

// 	log.Println(userAccount)
// }

// func TestServiceUpdateUserAccountEmail(t *testing.T) {
// 	err := testServiceUser.UpdateUserAccountEmail("newflmarsilfromservice@student.42.fr", testIdAccountOne)
// 	if err == nil {
// 		log.Println("User account has been updated from service")
// 	} else {
// 		log.Fatalf("Error : UpdateUserAccount : %v\n", err)
// 	}
// }

// func TestServiceUpdateUserAccountPseudo(t *testing.T) {
// 	err := testServiceUser.UserAccount().UpdateUserAccountPseudo("flmarsilfromservice", testIdAccountOne)
// 	if err == nil {
// 		log.Println("User account has been updated from service")
// 	} else {
// 		log.Fatalf("Error : UpdateUserAccount : %v\n", err)
// 	}
// }

// func TestServiceUpdateUserAccountHashedPassword(t *testing.T) {
// 	newPwd := "eaeff72b6055aa253c6470f4cb272f841c5eb8d613d6926f7ce9f6e9ee90c54a"
// 	err := testServiceUser.UpdateUserAccountHashedPassword(testAccountOne.Password, newPwd, testIdAccountOne)
// 	if err == nil {
// 		log.Println("User account has been updated from service")
// 	} else {
// 		log.Fatalf("Error : UpdateUserAccount : %v\n", err)
// 	}
// }

// func TestServiceUpdateUserLikes(t *testing.T) {
// 	err := testServiceUser.UpdateUserLikes(testIdAccountOne, testIdAccountTwo, false)
// 	if err == nil {
// 		log.Println("User like has been updated from service")
// 	} else {
// 		log.Fatalf("Error : UpdateUserLike : %v\n", err)
// 	}
// }

// func TestServiceUpdateUserStatusConnection(t *testing.T) {
// 	err := testServiceUser.UserInteraction().UpdateUserStatusConnection(testIdAccountOne, true)
// 	if err == nil {
// 		log.Println("User status connection has been updated from service")
// 	} else {
// 		log.Fatalf("Error : UpdateUserStatusConnection : %v\n", err)
// 	}
// }

// func TestServiceUpdateUserBlocks(t *testing.T) {
// 	err := testServiceUser.UpdateUserBlocks(testIdAccountOne, testIdAccountTwo, false)
// 	if err == nil {
// 		log.Println("User blocked status has been updated from service")
// 	} else {
// 		log.Fatalf("Error : UpdateUserBlocks : %v\n", err)
// 	}
// }

// func TestServiceUpdateUserGender(t *testing.T) {
// 	err := testServiceUser.UpdateUserGender(testIdAccountOne, "femme")
// 	if err == nil {
// 		log.Println("User gender has been updated from service")
// 	} else {
// 		log.Fatalf("Error : UpdateUserGender : %v\n", err)
// 	}
// }

// func TestServiceUpdateUserBio(t *testing.T) {
// 	err := testServiceUser.UpdateUserBio(testIdAccountOne, "This is my new Bio")
// 	if err == nil {
// 		log.Println("User bio has been updated from service")
// 	} else {
// 		log.Fatalf("Error : UpdateUserBio : %v\n", err)
// 	}
// }

// func TestServiceUpdateUserInterestInRelationship(t *testing.T) {
// 	err := testServiceUser.UpdateUserInterestInRelationship(testIdAccountOne, "cdd")
// 	if err == nil {
// 		log.Println("User interest in relationship has been updated from service")
// 	} else {
// 		log.Fatalf("Error : UpdateUserInterestInRelationship : %v\n", err)
// 	}
// }

// func TestServiceUpdateUpdateUserInterestInGender(t *testing.T) {
// 	err := testServiceUser.UpdateUserInterestInGender(testIdAccountOne, "femme")
// 	if err == nil {
// 		log.Println("User interest in gender has been updated from service")
// 	} else {
// 		log.Fatalf("Error : UpdateUpdateUserInterestInGender : %v\n", err)
// 	}
// }

// func TestServiceUpdateUserSize(t *testing.T) {
// 	err := testServiceUser.UpdateUserSize(testIdAccountOne, "180")
// 	if err == nil {
// 		log.Println("User size has been updated from service")
// 	} else {
// 		log.Fatalf("Error : UpdateUserSize : %v\n", err)
// 	}
// }

// func TestServiceUpdateUserAstrosign(t *testing.T) {
// 	err := testServiceUser.UpdateUserAstrosign(testIdAccountOne, "scorpion")
// 	if err == nil {
// 		log.Println("User astrosign has been updated from service")
// 	} else {
// 		log.Fatalf("Error : UpdateUserAstrosign : %v\n", err)
// 	}
// }

// func TestServiceUpdateUserOrigin(t *testing.T) {
// 	err := testServiceUser.UpdateUserOrigin(testIdAccountOne, "latines")
// 	if err == nil {
// 		log.Println("User origin has been updated from service")
// 	} else {
// 		log.Fatalf("Error : UpdateUserOrigin : %v\n", err)
// 	}
// }

// func TestServiceUpdateUserHairCut(t *testing.T) {
// 	err := testServiceUser.UpdateUserHairCut(testIdAccountOne, "courts")
// 	if err == nil {
// 		log.Println("User hair cut has been updated from service")
// 	} else {
// 		log.Fatalf("Error : UpdateUserHairCut : %v\n", err)
// 	}
// }

// func TestServiceUpdateUserHairColor(t *testing.T) {
// 	err := testServiceUser.UpdateUserHairColor(testIdAccountOne, "noir")
// 	if err == nil {
// 		log.Println("User hair color has been updated from service")
// 	} else {
// 		log.Fatalf("Error : UpdateUserHairColor : %v\n", err)
// 	}
// }

// func TestServiceUpdateUserHairStyle(t *testing.T) {
// 	err := testServiceUser.UpdateUserHairStyle(testIdAccountOne, "")
// 	if err == nil {
// 		log.Println("User hair style has been updated from service")
// 	} else {
// 		log.Fatalf("Error : UpdateUserHairStyle : %v\n", err)
// 	}
// }

// func TestServicerUpdateUserEyesColor(t *testing.T) {
// 	err := testServiceUser.UpdateUserEyesColor(testIdAccountOne, "marrons")
// 	if err == nil {
// 		log.Println("User eyes color has been updated from service")
// 	} else {
// 		log.Fatalf("Error : UpdateUserEyesColor : %v\n", err)
// 	}
// }

// func TestServiceUpdateUserShape(t *testing.T) {
// 	err := testServiceUser.UpdateUserShape(testIdAccountOne, "body-builder")
// 	if err == nil {
// 		log.Println("User shape has been updated from service")
// 	} else {
// 		log.Fatalf("Error : UpdateUserShape : %v\n", err)
// 	}
// }
