package user_interaction

import (
	"matcha/srcs/requirements/server/internal/adapters/framework/right/postgres"
)

type ServiceUserInteraction interface {
	UpdateUserBlocks(user_id_given, user_id_received uint64, like bool) error
	UpdateUserStatusConnection(user_id uint64, status bool) error
	UpdateUserLikes(user_id_given, user_id_received uint64, like bool) error
}

type UserInteraction struct {
	Repo postgres.Repository
}
