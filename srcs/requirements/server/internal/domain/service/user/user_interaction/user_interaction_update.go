package user_interaction

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (u *UserInteraction) UpdateUserBlocks(user_id_given, user_id_received uint64, block bool) error {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id_given)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Given user id could not be check or does not exist")
	}

	idExist, err = u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id_given)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Received user id could not be check or does not exist")
	}

	currentStatusBlock, err := u.Repo.UserQuery().UserInteraction().GetIfOtherUserBlockedUser(user_id_received, user_id_given)
	if err != nil {
		return status.Errorf(codes.Internal, "Cannot check current block status")
	}

	if !block && !currentStatusBlock {
		return status.Errorf(codes.PermissionDenied, "User is not blocked yet")
	} else if block && currentStatusBlock {
		return status.Errorf(codes.PermissionDenied, "User is already blocked")
	}

	if block {
		err := u.Repo.UserQuery().UserInteraction().CreateUserBlock(user_id_given, user_id_received)
		if err != nil {
			return status.Errorf(codes.Internal, "Unable to like user")
		}
	} else {
		err := u.Repo.UserQuery().UserInteraction().DeleteUserBlock(user_id_given, user_id_received)
		if err != nil {
			return status.Errorf(codes.Internal, "Unable to unlike user")
		}
	}

	return nil
}

func (u *UserInteraction) UpdateUserStatusConnection(user_id uint64, connectionStatus bool) error {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Given user id could not be check or does not exist")
	}

	currentStatusConnection, err := u.Repo.UserQuery().UserDetails().GetUserStatusConnection(user_id)
	if err != nil {
		return status.Errorf(codes.Internal, "Cannot get user current status connection")
	}

	if !connectionStatus && !currentStatusConnection {
		return status.Errorf(codes.PermissionDenied, "Status connection is already disconnected")
	} else if connectionStatus && currentStatusConnection {
		return status.Errorf(codes.PermissionDenied, "Status connection is already connected")
	}

	err = u.Repo.UserQuery().UserDetails().UpdateUserStatusConnection(user_id, connectionStatus)
	if err != nil {
		return status.Errorf(codes.Internal, "Cannot update status connection")
	}

	return nil
}

func (u *UserInteraction) UpdateUserLikes(user_id_given, user_id_received uint64, like bool) error {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id_given)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Given user id could not be check or does not exist")
	}

	idExist, err = u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id_given)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Received user id could not be check or does not exist")
	}

	currentStatusLike, err := u.Repo.UserQuery().UserInteraction().GetIfOtherUserLikedUser(user_id_received, user_id_given)
	if err != nil {
		return status.Errorf(codes.Internal, "Cannot check current like status")
	}

	if !like && !currentStatusLike {
		return status.Errorf(codes.PermissionDenied, "User is not liked yet")
	} else if like && currentStatusLike {
		return status.Errorf(codes.PermissionDenied, "User is already liked")
	}

	if like {
		err := u.Repo.UserQuery().UserInteraction().CreateUserLike(user_id_given, user_id_received)
		if err != nil {
			return status.Errorf(codes.Internal, "Unable to like user")
		}
	} else {
		err := u.Repo.UserQuery().UserInteraction().DeleteUserLike(user_id_given, user_id_received)
		if err != nil {
			return status.Errorf(codes.Internal, "Unable to unlike user")
		}
	}

	return nil
}
