package user

import (
	"matcha/srcs/requirements/server/internal/adapters/framework/right/postgres"
	sua "matcha/srcs/requirements/server/internal/domain/service/user/user_account"
	sud "matcha/srcs/requirements/server/internal/domain/service/user/user_details"
	sui "matcha/srcs/requirements/server/internal/domain/service/user/user_interaction"
)

type User interface {
	UserAccount() sua.ServiceUserAccount
	UserDetails() sud.ServiceUserDetails
	UserInteraction() sui.ServiceUserInteraction
}

type user struct {
	repo postgres.Repository
}

func NewServiceUser(repo postgres.Repository) User {
	return &user{repo: repo}
}

func (u *user) UserAccount() sua.ServiceUserAccount {
	return &sua.UserAccount{Repo: u.repo}
}

func (u *user) UserDetails() sud.ServiceUserDetails {
	return &sud.UserDetails{Repo: u.repo}
}

func (u *user) UserInteraction() sui.ServiceUserInteraction {
	return &sui.UserInteraction{Repo: u.repo}
}
