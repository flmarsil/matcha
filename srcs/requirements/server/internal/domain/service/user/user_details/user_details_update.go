package user_details

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const user_details_regexp = "^[a-z0-9+-]+$"

func (u *UserDetails) UpdateUserShape(user_id uint64, shape string) error {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	newShape, err := inputCleaner(shape, user_details_regexp)
	if err != nil {
		return status.Errorf(codes.InvalidArgument, "Incorrect input expression")
	}

	err = u.Repo.UserQuery().UserDetails().UpdateUserShape(user_id, *newShape)
	if err != nil {
		return status.Errorf(codes.Internal, "Shape could not be modified : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserEyesColor(user_id uint64, eyes_color string) error {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	newEyesColor, err := inputCleaner(eyes_color, user_details_regexp)
	if err != nil {
		return status.Errorf(codes.InvalidArgument, "Incorrect input expression")
	}

	err = u.Repo.UserQuery().UserDetails().UpdateUserEyesColor(user_id, *newEyesColor)
	if err != nil {
		return status.Errorf(codes.Internal, "Eyes color could not be modified : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserHairStyle(user_id uint64, hair_style string) error {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	newHairStyle, err := inputCleaner(hair_style, user_details_regexp)
	if err != nil {
		return status.Errorf(codes.InvalidArgument, "Incorrect input expression")
	}

	err = u.Repo.UserQuery().UserDetails().UpdateUserHairStyle(user_id, *newHairStyle)
	if err != nil {
		return status.Errorf(codes.Internal, "Hair style could not be modified : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserHairColor(user_id uint64, hair_color string) error {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	newHairColor, err := inputCleaner(hair_color, user_details_regexp)
	if err != nil {
		return status.Errorf(codes.InvalidArgument, "Incorrect input expression")
	}

	err = u.Repo.UserQuery().UserDetails().UpdateUserHairColor(user_id, *newHairColor)
	if err != nil {
		return status.Errorf(codes.Internal, "Hair color could not be modified : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserHairCut(user_id uint64, hair_cut string) error {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	newHairCut, err := inputCleaner(hair_cut, user_details_regexp)
	if err != nil {
		return status.Errorf(codes.InvalidArgument, "Incorrect input expression")
	}

	err = u.Repo.UserQuery().UserDetails().UpdateUserHairCut(user_id, *newHairCut)
	if err != nil {
		return status.Errorf(codes.Internal, "Hair cut could not be modified : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserOrigin(user_id uint64, origin string) error {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	newOrigin, err := inputCleaner(origin, user_details_regexp)
	if err != nil {
		return status.Errorf(codes.InvalidArgument, "Incorrect input expression")
	}

	err = u.Repo.UserQuery().UserDetails().UpdateUserOrigin(user_id, *newOrigin)
	if err != nil {
		return status.Errorf(codes.Internal, "Origin could not be modified : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserAstrosign(user_id uint64, astrosign string) error {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	newAstrosign, err := inputCleaner(astrosign, user_details_regexp)
	if err != nil {
		return status.Errorf(codes.InvalidArgument, "Incorrect input expression")
	}

	err = u.Repo.UserQuery().UserDetails().UpdateUserAstrosign(user_id, *newAstrosign)
	if err != nil {
		return status.Errorf(codes.Internal, "Astrosign could not be modified : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserSize(user_id uint64, size string) error {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	newSize, err := inputCleaner(size, user_details_regexp)
	if err != nil {
		return status.Errorf(codes.InvalidArgument, "Incorrect input expression")
	}

	err = u.Repo.UserQuery().UserDetails().UpdateUserSize(user_id, *newSize)
	if err != nil {
		return status.Errorf(codes.Internal, "Size could not be modified : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserInterestInGender(user_id uint64, interest string) error {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	newInterest, err := inputCleaner(interest, user_details_regexp)
	if err != nil {
		return status.Errorf(codes.InvalidArgument, "Incorrect input expression")
	}

	err = u.Repo.UserQuery().UserDetails().UpdateUserInterestInGender(user_id, *newInterest)
	if err != nil {
		return status.Errorf(codes.Internal, "Interest in gender could not be modified : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserInterestInRelationship(user_id uint64, interest string) error {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	newInterest, err := inputCleaner(interest, user_details_regexp)
	if err != nil {
		return status.Errorf(codes.InvalidArgument, "Incorrect input expression")
	}

	err = u.Repo.UserQuery().UserDetails().UpdateUserInterestInRelationship(user_id, *newInterest)
	if err != nil {
		return status.Errorf(codes.Internal, "Interest in relationship could not be modified : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserBio(user_id uint64, bio string) error {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	err = u.Repo.UserQuery().UserDetails().UpdateUserBio(user_id, bio)
	if err != nil {
		return status.Errorf(codes.Internal, "Bio could not be modified : %v", err)
	}

	return nil
}

func (u *UserDetails) UpdateUserGender(user_id uint64, gender string) error {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	newGender, err := inputCleaner(gender, user_details_regexp)
	if err != nil {
		return status.Errorf(codes.InvalidArgument, "Incorrect input expression")
	}

	err = u.Repo.UserQuery().UserDetails().UpdateUserGender(user_id, *newGender)
	if err != nil {
		return status.Errorf(codes.Internal, "Gender could not be modified : %v", err)
	}

	return nil
}
