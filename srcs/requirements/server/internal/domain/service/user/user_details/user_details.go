package user_details

import (
	"matcha/srcs/requirements/server/internal/adapters/framework/right/postgres"
	"matcha/srcs/requirements/server/internal/domain/model"
	"regexp"
	"strings"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ServiceUserDetails interface {
	GetUserDetails(user_pseudo string, requested_by_id uint64) (*model.UserDetails, error)

	UpdateUserGender(user_id uint64, gender string) error
	UpdateUserBio(user_id uint64, bio string) error
	UpdateUserInterestInRelationship(user_id uint64, interest string) error
	UpdateUserInterestInGender(user_id uint64, interest string) error
	UpdateUserSize(user_id uint64, size string) error
	UpdateUserAstrosign(user_id uint64, astrosign string) error
	UpdateUserOrigin(user_id uint64, origin string) error
	UpdateUserHairCut(user_id uint64, hair_cut string) error
	UpdateUserHairColor(user_id uint64, hair_color string) error
	UpdateUserHairStyle(user_id uint64, hair_style string) error
	UpdateUserEyesColor(user_id uint64, eye_color string) error
	UpdateUserShape(user_id uint64, shape string) error
	// UpdateUserPhotos(user_id uint64, content []byte) error

}

type UserDetails struct {
	Repo postgres.Repository
}

func inputCleaner(input, exp string) (*string, error) {
	reg := regexp.MustCompile(exp)

	input = strings.ToLower(input)

	check := reg.MatchString(input)
	if !check {
		return nil, status.Errorf(codes.InvalidArgument, "Incorrect input")
	}

	return &input, nil
}
