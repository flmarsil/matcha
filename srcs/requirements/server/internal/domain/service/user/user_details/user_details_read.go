package user_details

import (
	"matcha/srcs/requirements/server/internal/domain/model"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const user_pseudo_regexp = "^([a-z]|[A-Z]|[0-9]){4,15}$"

func (u *UserDetails) GetUserDetails(user_pseudo string, requested_by_id uint64) (*model.UserDetails, error) {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(requested_by_id)
	if err != nil || !idExist {
		return nil, status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	userPseudo, err := inputCleaner(user_pseudo, user_pseudo_regexp)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "Incorrect pseudo expression")
	}

	id, err := u.Repo.UserQuery().UserAccount().GetUserAccountIdByPseudo(*userPseudo)
	if err != nil {
		return nil, status.Errorf(codes.NotFound, "Requested user account doesn't exist: %v", err)
	}

	isBlocked, err := u.Repo.UserQuery().UserInteraction().GetIfOtherUserBlockedUser(requested_by_id, id)
	if err != nil || isBlocked {
		return nil, status.Errorf(codes.PermissionDenied, "Requested user has blocked requesting user")
	}

	account, _ := u.Repo.UserQuery().UserAccount().GetUserAccountPublic(id)
	bio, _ := u.Repo.UserQuery().UserDetails().GetUserBio(id)
	interestInRelationship, _ := u.Repo.UserQuery().UserDetails().GetUserInterestInRelationship(id)
	interestInGender, _ := u.Repo.UserQuery().UserDetails().GetUserInterestInGender(id)
	astrosign, _ := u.Repo.UserQuery().UserDetails().GetUserAstrosign(id)
	origin, _ := u.Repo.UserQuery().UserDetails().GetUserOrigin(id)
	hairCut, _ := u.Repo.UserQuery().UserDetails().GetUserHairCut(id)
	hairColor, _ := u.Repo.UserQuery().UserDetails().GetUserHairColor(id)
	hairStyle, _ := u.Repo.UserQuery().UserDetails().GetUserHairStyle(id)
	eyesColor, _ := u.Repo.UserQuery().UserDetails().GetUserEyesColor(id)
	shape, _ := u.Repo.UserQuery().UserDetails().GetUserShape(id)
	size, _ := u.Repo.UserQuery().UserDetails().GetUserSize(id)
	like, _ := u.Repo.UserQuery().UserInteraction().GetIfOtherUserLikedUser(requested_by_id, id)
	statusConnection, _ := u.Repo.UserQuery().UserDetails().GetUserStatusConnection(id)
	// photos := u.Repo.UserQuery().

	details := model.UserDetails{
		Pseudo:                 account.Pseudo,
		Location:               account.Location,
		Birthday:               account.Birthday,
		Popularity:             account.Popularity,
		LastConnection:         account.LastConnection,
		Bio:                    *bio,
		InterestInRelationship: *interestInRelationship,
		InterestInGender:       *interestInGender,
		Astrosign:              *astrosign,
		Origins:                *origin,
		HairCut:                *hairCut,
		HairColor:              *hairColor,
		HairStyle:              *hairStyle,
		EyesColor:              *eyesColor,
		Shape:                  *shape,
		Size:                   *size,
		LikedVisitor:           like,
		StatusConnection:       statusConnection,
		// Photos: *photos,
	}

	err = u.Repo.UserQuery().UserInteraction().CreateUserVisit(requested_by_id, id)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Cannot add visit to requested user : %v", err)
	}

	return &details, nil
}
