package user_account

import (
	"time"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	user_email_regexp    = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+[a-zA-Z0-9-.]+$"
	user_hash_pwd_regexp = "^[a-f0-9]{64}$"
	user_pseudo_regexp   = "^([a-z]|[A-Z]|[0-9]){4,15}$"
)

func (u *UserAccount) UpdateUserAccountEmail(new_email string, user_id uint64) error {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	newEmail, err := inputCleaner(new_email, user_email_regexp)
	if err != nil {
		return status.Errorf(codes.InvalidArgument, "Incorrect email expression")
	}

	id, _ := u.Repo.UserQuery().UserAccount().GetUserAccountIdByEmail(*newEmail)
	if id != 0 {
		return status.Errorf(codes.PermissionDenied, "Email already exists")
	} else {
		err := u.Repo.UserQuery().UserAccount().UpdateUserAccountEmail(user_id, *newEmail)
		if err != nil {
			return status.Errorf(codes.Internal, "Email could not be modified : %v", err)
		}
	}

	return nil
}

func (u *UserAccount) UpdateUserAccountPseudo(new_pseudo string, user_id uint64) error {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	newPseudo, err := inputCleaner(new_pseudo, user_pseudo_regexp)
	if err != nil {
		return status.Errorf(codes.InvalidArgument, "Incorrect pseudo expression")
	}

	id, _ := u.Repo.UserQuery().UserAccount().GetUserAccountIdByPseudo(*newPseudo)
	if id != 0 {
		return status.Errorf(codes.PermissionDenied, "Pseudo already exists")
	} else {
		err := u.Repo.UserQuery().UserAccount().UpdateUserAccountPseudo(user_id, *newPseudo)
		if err != nil {
			return status.Errorf(codes.Internal, "Pseudo could not be modified : %v", err)
		}
	}

	return nil
}

func (u *UserAccount) UpdateUserAccountHashedPassword(old_pwd string, new_pwd string, user_id uint64) error {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	oldHash, err := inputCleaner(old_pwd, user_hash_pwd_regexp)
	if err != nil {
		return status.Errorf(codes.InvalidArgument, "Incorrect old password expression")
	}

	newHash, err := inputCleaner(new_pwd, user_hash_pwd_regexp)
	if err != nil {
		return status.Errorf(codes.InvalidArgument, "Incorrect new password expression")
	}

	currentHash, err := u.Repo.UserQuery().UserAccount().GetUserAccountHashedPasswordById(user_id)
	if err != nil {
		return status.Errorf(codes.NotFound, "Hashed password not found by id")
	}

	if *oldHash != *currentHash {
		return status.Errorf(codes.InvalidArgument, "Old password does not match")
	} else {
		// send email confirmation, wait ... Si OK changer passwd
		err = u.Repo.UserQuery().UserAccount().UpdateUserAccountHashedPassword(user_id, *newHash)
		if err != nil {
			return status.Errorf(codes.Internal, "New password could not be modified : %v", err)
		}
	}

	return nil
}

func (u *UserAccount) UpdateUserAccountBirthday(new_birthday time.Time, user_id uint64) error {
	if new_birthday.IsZero() {
		return status.Errorf(codes.InvalidArgument, "New birthday and user id are mandatory")
	}

	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	err = u.Repo.UserQuery().UserAccount().UpdateUserAccountBirthday(user_id, new_birthday)
	if err != nil {
		return status.Errorf(codes.Internal, "New birthday could not be modified : %v", err)
	}

	return nil
}

func (u *UserAccount) UpdateUserAccountLocation(new_location string, user_id uint64) error {
	if new_location == "" {
		return status.Errorf(codes.InvalidArgument, "New location and user id are mandatory")
	}

	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id)
	if err != nil || !idExist {
		return status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	// change location

	return nil
}
