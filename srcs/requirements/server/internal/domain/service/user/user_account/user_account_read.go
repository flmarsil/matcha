package user_account

import (
	"matcha/srcs/requirements/server/internal/domain/model"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (u *UserAccount) GetUserAccount(user_id, requested_by_id uint64) (*model.UserAccount, error) {
	idExist, err := u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(user_id)
	if err != nil || !idExist {
		return nil, status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	idExist, err = u.Repo.UserQuery().UserInteraction().GetIfUserIdExist(requested_by_id)
	if err != nil || !idExist {
		return nil, status.Errorf(codes.PermissionDenied, "Id user could not be check or does not exist")
	}

	if user_id != requested_by_id {
		return nil, status.Errorf(codes.PermissionDenied, "User is not authorized")
	}

	account, err := u.Repo.UserQuery().UserAccount().GetUserAccountPrivate(user_id)
	if err != nil {
		return nil, status.Errorf(codes.NotFound, "requested user account doesn't exist: %v", err)
	}

	return account, nil
}
