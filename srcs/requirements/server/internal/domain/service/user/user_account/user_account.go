package user_account

import (
	"matcha/srcs/requirements/server/internal/adapters/framework/right/postgres"
	"matcha/srcs/requirements/server/internal/domain/model"
	"regexp"
	"strings"
	"time"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ServiceUserAccount interface {
	GetUserAccount(user_id, requested_by_id uint64) (*model.UserAccount, error)

	UpdateUserAccountEmail(new_email string, user_id uint64) error
	UpdateUserAccountPseudo(new_pseudo string, user_id uint64) error
	UpdateUserAccountHashedPassword(old_pwd string, new_pwd string, user_id uint64) error
	UpdateUserAccountBirthday(new_birthday time.Time, user_id uint64) error
	UpdateUserAccountLocation(new_location string, user_id uint64) error
}

type UserAccount struct {
	Repo postgres.Repository
}

func inputCleaner(input, exp string) (*string, error) {
	reg := regexp.MustCompile(exp)

	input = strings.ToLower(input)

	check := reg.MatchString(input)
	if !check {
		return nil, status.Errorf(codes.InvalidArgument, "Incorrect input")
	}

	return &input, nil
}
