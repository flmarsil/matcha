package dto

import "time"

type UserAccount struct {
	Email       string
	OldPassword string
	NewPassword string
	Pseudo      string
	Birthday    time.Time
	Location    string
}

type UserDetails struct {
	Bio                    string
	Photos                 []string
	InterestInRelationship string
	InterestInGender       string
	Astrosign              string
	Origins                string
	HairCut                string
	HairColor              string
	HairStyle              string
	EyesColor              string
	Shape                  string
	Size                   string
}
