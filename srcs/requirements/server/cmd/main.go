package main

import (
	"matcha/srcs/requirements/server/internal/adapters/framework/right/postgres"
	service "matcha/srcs/requirements/server/internal/domain/service/user"

	_ "github.com/lib/pq"
)

func main() {
	// db initialisation
	db, err := postgres.NewDB()
	if err != nil {
		return
	}
	defer postgres.CloseDB()

	// register all services
	repo := postgres.NewRepository(db)
	user := service.NewServiceUser(repo)

	if user == nil {
		return
	}

	// create api with all services

	// starting grpc server with all services
	for {

	}
}
