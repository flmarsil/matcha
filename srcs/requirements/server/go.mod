module matcha/srcs/requirements/server

go 1.17

require (
	github.com/lib/pq v1.10.4
	google.golang.org/grpc v1.44.0
)

require (
	github.com/golang/protobuf v1.4.3 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
