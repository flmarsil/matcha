# Matcha (en cours ...)

Création d'une application web type site de rencontres.

- Conception d'une base de donnees relationelle.
- Mise en place d'une "clean architecture" dans le but de réduire les dépendances de la logique metier et de fournir une application stable au cours de ses evolutions, de ses tests mais egalement lors de changements ou mises a jour de ressources externes.
- Implementation du protocol gRPC pour garantir une communication rapide et legere entre les differents microservices.

#### Stack :
- Front : Vue JS / TypeScript
- Back : Golang, gRPC
- Proxy : Traefik
- Db : PostgreSQL, Redis, Elastic-search
- CI/CD: Docker