-- genders
CREATE TYPE gender AS ENUM ('homme', 'femme');

CREATE TABLE IF NOT EXISTS "t_genders" (
  "id_gender" smallint PRIMARY KEY,
  "gender_content" gender UNIQUE NOT NULL 
);

INSERT INTO t_genders(id_gender,gender_content) VALUES (1, 'homme');
INSERT INTO t_genders(id_gender,gender_content) VALUES (2, 'femme');

-- sizes
CREATE TYPE size AS ENUM ('120', '125', '130', '135', '140', '145', '150', '155', '160', '165', '170', '175', '180', '185', '190', '195', '200', '205', '210', '215', '220');

CREATE TABLE IF NOT EXISTS "t_sizes" (
  "id_size" smallint PRIMARY KEY,
  "size_content" size UNIQUE NOT NULL 
);

INSERT INTO t_sizes(id_size,size_content) VALUES (1, '120');
INSERT INTO t_sizes(id_size,size_content) VALUES (2, '125');
INSERT INTO t_sizes(id_size,size_content) VALUES (3, '130');
INSERT INTO t_sizes(id_size,size_content) VALUES (4, '135');
INSERT INTO t_sizes(id_size,size_content) VALUES (5, '140');
INSERT INTO t_sizes(id_size,size_content) VALUES (6, '145');
INSERT INTO t_sizes(id_size,size_content) VALUES (7, '150');
INSERT INTO t_sizes(id_size,size_content) VALUES (8, '155');
INSERT INTO t_sizes(id_size,size_content) VALUES (9, '160');
INSERT INTO t_sizes(id_size,size_content) VALUES (10, '165');
INSERT INTO t_sizes(id_size,size_content) VALUES (11, '170');
INSERT INTO t_sizes(id_size,size_content) VALUES (12, '175');
INSERT INTO t_sizes(id_size,size_content) VALUES (13, '180');
INSERT INTO t_sizes(id_size,size_content) VALUES (14, '185');
INSERT INTO t_sizes(id_size,size_content) VALUES (15, '190');
INSERT INTO t_sizes(id_size,size_content) VALUES (16, '195');
INSERT INTO t_sizes(id_size,size_content) VALUES (17, '200');
INSERT INTO t_sizes(id_size,size_content) VALUES (18, '205');
INSERT INTO t_sizes(id_size,size_content) VALUES (19, '210');
INSERT INTO t_sizes(id_size,size_content) VALUES (20, '215');
INSERT INTO t_sizes(id_size,size_content) VALUES (21, '220');

-- shapes
CREATE TYPE shape AS ENUM ('mince', 'costaud', 'muscle', 'body-builder', 'enrobe', 'normal');

CREATE TABLE IF NOT EXISTS "t_shapes" (
  "id_shape" smallint PRIMARY KEY,
  "shape_content" shape NOT NULL UNIQUE
);

INSERT INTO t_shapes(id_shape,shape_content) VALUES (1, 'mince');
INSERT INTO t_shapes(id_shape,shape_content) VALUES (2, 'costaud');
INSERT INTO t_shapes(id_shape,shape_content) VALUES (3, 'muscle');
INSERT INTO t_shapes(id_shape,shape_content) VALUES (4, 'body-builder');
INSERT INTO t_shapes(id_shape,shape_content) VALUES (5, 'enrobe');
INSERT INTO t_shapes(id_shape,shape_content) VALUES (6, 'normal');


-- eyes
CREATE TYPE eyescolor AS ENUM ('noirs', 'marrons', 'noisettes', 'bleus', 'verts', 'gris', 'vairons');

CREATE TABLE IF NOT EXISTS "t_eyes" (
  "id_eye" smallint PRIMARY KEY,
  "eye_content" eyescolor UNIQUE NOT NULL
);

INSERT INTO t_eyes(id_eye,eye_content) VALUES (1, 'noirs');
INSERT INTO t_eyes(id_eye,eye_content) VALUES (2, 'marrons');
INSERT INTO t_eyes(id_eye,eye_content) VALUES (3, 'noisettes');
INSERT INTO t_eyes(id_eye,eye_content) VALUES (4, 'bleus');
INSERT INTO t_eyes(id_eye,eye_content) VALUES (5, 'verts');
INSERT INTO t_eyes(id_eye,eye_content) VALUES (6, 'gris');

-- hairs color
CREATE TYPE haircolor AS ENUM ('blanc', 'blond', 'noir', 'gris', 'chatain', 'colores', 'poivre-et-sel', 'roux', 'platine', 'brun');

CREATE TABLE IF NOT EXISTS "t_hairs_colors" (
  "id_hair_color" smallint PRIMARY KEY,
  "hair_color_content" haircolor UNIQUE NOT NULL
);

INSERT INTO t_hairs_colors(id_hair_color,hair_color_content) VALUES (1, 'blanc');
INSERT INTO t_hairs_colors(id_hair_color,hair_color_content) VALUES (2, 'blond');
INSERT INTO t_hairs_colors(id_hair_color,hair_color_content) VALUES (3, 'noir');
INSERT INTO t_hairs_colors(id_hair_color,hair_color_content) VALUES (4, 'gris');
INSERT INTO t_hairs_colors(id_hair_color,hair_color_content) VALUES (5, 'chatain');
INSERT INTO t_hairs_colors(id_hair_color,hair_color_content) VALUES (6, 'colores');
INSERT INTO t_hairs_colors(id_hair_color,hair_color_content) VALUES (7, 'poivre-et-sel');
INSERT INTO t_hairs_colors(id_hair_color,hair_color_content) VALUES (8, 'roux');
INSERT INTO t_hairs_colors(id_hair_color,hair_color_content) VALUES (9, 'platine');
INSERT INTO t_hairs_colors(id_hair_color,hair_color_content) VALUES (10, 'brun');

-- hairs cut
CREATE TYPE haircut AS ENUM ('rases', 'courts', 'mi-longs', 'longs');

CREATE TABLE IF NOT EXISTS "t_hairs_cuts" (
  "id_hair_cut" smallint PRIMARY KEY,
  "hair_cut_content" haircut UNIQUE NOT NULL
);

INSERT INTO t_hairs_cuts(id_hair_cut,hair_cut_content) VALUES (1, 'rases');
INSERT INTO t_hairs_cuts(id_hair_cut,hair_cut_content) VALUES (2, 'courts');
INSERT INTO t_hairs_cuts(id_hair_cut,hair_cut_content) VALUES (3, 'mi-longs');
INSERT INTO t_hairs_cuts(id_hair_cut,hair_cut_content) VALUES (4, 'longs');

-- hairs style
CREATE TYPE hairstyle AS ENUM ('raides', 'afro', 'ondules', 'dreads', 'boucles', 'crete', 'frises', 'tresses-afro');

CREATE TABLE IF NOT EXISTS "t_hairs_styles" (
  "id_hair_style" smallint PRIMARY KEY,
  "hair_style_content" hairstyle UNIQUE NOT NULL
);

INSERT INTO t_hairs_styles(id_hair_style,hair_style_content) VALUES (1, 'raides');
INSERT INTO t_hairs_styles(id_hair_style,hair_style_content) VALUES (2, 'afro');
INSERT INTO t_hairs_styles(id_hair_style,hair_style_content) VALUES (3, 'ondules');
INSERT INTO t_hairs_styles(id_hair_style,hair_style_content) VALUES (4, 'dreads');
INSERT INTO t_hairs_styles(id_hair_style,hair_style_content) VALUES (5, 'boucles');
INSERT INTO t_hairs_styles(id_hair_style,hair_style_content) VALUES (6, 'crete');
INSERT INTO t_hairs_styles(id_hair_style,hair_style_content) VALUES (7, 'frises');
INSERT INTO t_hairs_styles(id_hair_style,hair_style_content) VALUES (8, 'tresses-afro');


-- origins
CREATE TYPE origin AS ENUM ('europeenes', 'metisses', 'moyen-orient', 'afro', 'eurasiennes', 'maghrebines', 'latines', 'asiatiques', 'antillaises');

CREATE TABLE IF NOT EXISTS "t_origins" (
  "id_origin" smallint PRIMARY KEY,
  "origin_content" origin UNIQUE NOT NULL
);

INSERT INTO t_origins(id_origin,origin_content) VALUES (1, 'europeenes');
INSERT INTO t_origins(id_origin,origin_content) VALUES (2, 'metisses');
INSERT INTO t_origins(id_origin,origin_content) VALUES (3, 'moyen-orient');
INSERT INTO t_origins(id_origin,origin_content) VALUES (4, 'afro');
INSERT INTO t_origins(id_origin,origin_content) VALUES (5, 'eurasiennes');
INSERT INTO t_origins(id_origin,origin_content) VALUES (6, 'maghrebines');
INSERT INTO t_origins(id_origin,origin_content) VALUES (7, 'latines');
INSERT INTO t_origins(id_origin,origin_content) VALUES (8, 'asiatiques');
INSERT INTO t_origins(id_origin,origin_content) VALUES (9, 'antillaises');

-- astrosigns
CREATE TYPE astrosign AS ENUM ('belier', 'taureau', 'gemeaux', 'cancer', 'lion', 'vierge', 'balance', 'scorpion', 'sagittaire', 'capricorne', 'verseau', 'poissons');

CREATE TABLE IF NOT EXISTS "t_astrosigns" (
  "id_astrosign" smallint PRIMARY KEY,
  "astrosign_content" astrosign UNIQUE NOT NULL
);

INSERT INTO t_astrosigns(id_astrosign,astrosign_content) VALUES (1, 'belier');
INSERT INTO t_astrosigns(id_astrosign,astrosign_content) VALUES (2, 'taureau');
INSERT INTO t_astrosigns(id_astrosign,astrosign_content) VALUES (3, 'gemeaux');
INSERT INTO t_astrosigns(id_astrosign,astrosign_content) VALUES (4, 'cancer');
INSERT INTO t_astrosigns(id_astrosign,astrosign_content) VALUES (5, 'lion');
INSERT INTO t_astrosigns(id_astrosign,astrosign_content) VALUES (6, 'vierge');
INSERT INTO t_astrosigns(id_astrosign,astrosign_content) VALUES (7, 'balance');
INSERT INTO t_astrosigns(id_astrosign,astrosign_content) VALUES (8, 'scorpion');
INSERT INTO t_astrosigns(id_astrosign,astrosign_content) VALUES (9, 'sagittaire');
INSERT INTO t_astrosigns(id_astrosign,astrosign_content) VALUES (10, 'capricorne');
INSERT INTO t_astrosigns(id_astrosign,astrosign_content) VALUES (11, 'verseau');
INSERT INTO t_astrosigns(id_astrosign,astrosign_content) VALUES (12, 'poissons');

-- relationship
CREATE TYPE relationship AS ENUM ('cdi', 'cdd', 'interim');

CREATE TABLE IF NOT EXISTS "t_relationships" (
  "id_relationship" smallint PRIMARY KEY,
  "relationship_content" relationship UNIQUE NOT NULL
);

INSERT INTO t_relationships(id_relationship,relationship_content) VALUES (1, 'cdi');
INSERT INTO t_relationships(id_relationship,relationship_content) VALUES (2, 'cdd');
INSERT INTO t_relationships(id_relationship,relationship_content) VALUES (3, 'interim');

CREATE TABLE IF NOT EXISTS "t_users_genders" (
  "id_user_gender" smallserial PRIMARY KEY,
  "gender_id" smallint DEFAULT NULL,
  "user_account_id" smallint UNIQUE
);

CREATE TABLE IF NOT EXISTS "t_users_sizes" (
  "id_user_size" smallserial PRIMARY KEY,
  "size_id" smallint,
  "user_account_id" smallint UNIQUE
);

CREATE TABLE IF NOT EXISTS "t_users_shapes" (
  "id_user_shape" smallserial PRIMARY KEY,
  "shape_id" smallint,
  "user_account_id" smallint UNIQUE
);

CREATE TABLE IF NOT EXISTS "t_users_eyes" (
  "id_user_eye" smallserial PRIMARY KEY,
  "eye_id" smallint,
  "user_account_id" smallint UNIQUE
);

CREATE TABLE IF NOT EXISTS "t_users_hairs_styles" (
  "id_user_hair_style" smallserial PRIMARY KEY,
  "hair_style_id" smallint,
  "user_account_id" smallint UNIQUE
);

CREATE TABLE IF NOT EXISTS "t_users_hairs_colors" (
  "id_user_hair_color" smallserial PRIMARY KEY,
  "hair_color_id" smallint,
  "user_account_id" smallint UNIQUE
);

CREATE TABLE IF NOT EXISTS "t_users_hairs_cuts" (
  "id_user_hair_cut" smallserial PRIMARY KEY,
  "hair_cut_id" smallint,
  "user_account_id" smallint UNIQUE
);

CREATE TABLE IF NOT EXISTS "t_users_origins" (
  "id_user_origin" smallserial PRIMARY KEY,
  "origin_id" smallint,
  "user_account_id" smallint UNIQUE
);

CREATE TABLE IF NOT EXISTS "t_users_astrosigns" (
  "id_user_astrosign" smallserial PRIMARY KEY,
  "astrosign_id" smallint,
  "user_account_id" smallint UNIQUE
);

CREATE TABLE IF NOT EXISTS "t_users_interest_in_genders" (
  "id_user_interest_in_gender" smallserial PRIMARY KEY,
  "gender_id" smallint,
  "user_account_id" smallint UNIQUE
);

CREATE TABLE IF NOT EXISTS "t_users_interest_in_relationships" (
  "id_user_interest_in_relationship" smallserial PRIMARY KEY,
  "relationship_id" smallint,
  "user_account_id" smallint UNIQUE
);

CREATE TABLE IF NOT EXISTS "t_users_photos" (
  "id_user_photo" smallserial PRIMARY KEY,
  "user_photo_link" varchar,
  "user_account_id" smallint NOT NULL
);

CREATE TABLE IF NOT EXISTS "t_users_bios" (
  "id_user_bio" smallserial PRIMARY KEY,
  "user_bio_content" varchar(250),
  "user_account_id" smallint UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS "t_users_status_connections" (
  "id_user_status_connection" smallserial PRIMARY KEY,
  "status_connection" boolean,
  "user_account_id" smallint UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS "t_users_likes" (
  "id_user_like" smallserial PRIMARY KEY,
  "user_account_id_given" smallint NOT NULL,
  "user_account_id_received" smallint NOT NULL
);

CREATE TABLE IF NOT EXISTS "t_users_blocks" (
  "id_user_block" smallserial PRIMARY KEY,
  "user_account_id_given" smallint NOT NULL,
  "user_account_id_received" smallint NOT NULL
);

CREATE TABLE IF NOT EXISTS "t_users_visits" (
  "id_user_visit" smallserial PRIMARY KEY,
  "user_account_id_given" smallint NOT NULL,
  "user_account_id_received" smallint NOT NULL
);

CREATE TABLE IF NOT EXISTS "t_users_reports" (
  "id_user_report" smallserial PRIMARY KEY,
  "user_account_id_given" smallint NOT NULL,
  "user_account_id_received" smallint NOT NULL
);

-- users_accounts
CREATE TABLE IF NOT EXISTS "t_users_accounts" (
  "id_user_account" smallserial PRIMARY KEY,
  "user_account_email" varchar(100) UNIQUE NOT NULL,
  "user_account_hash_password" char(64) NOT NULL,
  "user_account_pseudo" varchar(20) UNIQUE NOT NULL,
  "user_account_birthday" date NOT NULL,
  "user_account_location" varchar(30),
  "user_account_popularity" smallint NOT NULL DEFAULT 5,
  "user_account_confirmation_code" varchar(5),
  "user_account_created_at" timestamp NOT NULL DEFAULT (now()),
  "user_account_last_connection_at" timestamp NOT NULL DEFAULT (now()),
  "user_account_verified_status" boolean NOT NULL DEFAULT FALSE
);

-- CHAT CONVERSATIONS 

CREATE TABLE IF NOT EXISTS "t_chat_conversations" (
  "id_chat_conversation" smallserial PRIMARY KEY,
  "user_account_id_given" smallint NOT NULL,
  "user_account_id_received" smallint NOT NULL,
  "chat_conversation_time_started" timestamp NOT NULL DEFAULT (now()),
  "chat_conversation_time_closed" timestamp
);

CREATE TABLE IF NOT EXISTS "t_chat_messages" (
  "id_chat_message" smallserial PRIMARY KEY,
  "chat_conversation_id" smallint NOT NULL,
  "user_account_id" smallint NOT NULL,
  "chat_message_content" varchar(500),
  "chat_message_time_stamp" timestamp NOT NULL DEFAULT (now())
);

-- TO DO : DELETE TOKEN_MANAGER - METTRE LE TOKEN EN CACHE POUR EVITER DE REQUETER LA DB 
CREATE TABLE IF NOT EXISTS "t_tokens_managers" (
  "id_token_manager" smallserial PRIMARY KEY,
  "token_content" varchar,
  "user_account_id" smallint UNIQUE NOT NULL
);
-- TO DO : DELETE TOKEN_MANAGER - METTRE LE TOKEN EN CACHE POUR EVITER DE REQUETER LA DB 
ALTER TABLE "t_tokens_managers" ADD FOREIGN KEY ("user_account_id") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;

-- RELATIONS

ALTER TABLE "t_chat_messages" ADD FOREIGN KEY ("chat_conversation_id") REFERENCES "t_chat_conversations" ("id_chat_conversation") ON DELETE CASCADE;
ALTER TABLE "t_chat_messages" ADD FOREIGN KEY ("user_account_id") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;

ALTER TABLE "t_chat_conversations" ADD FOREIGN KEY ("user_account_id_given") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;
ALTER TABLE "t_chat_conversations" ADD FOREIGN KEY ("user_account_id_received") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;

ALTER TABLE "t_users_status_connections" ADD FOREIGN KEY ("user_account_id") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;

ALTER TABLE "t_users_reports" ADD FOREIGN KEY ("user_account_id_given") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;
ALTER TABLE "t_users_reports" ADD FOREIGN KEY ("user_account_id_received") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;

ALTER TABLE "t_users_visits" ADD FOREIGN KEY ("user_account_id_given") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;
ALTER TABLE "t_users_visits" ADD FOREIGN KEY ("user_account_id_received") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;

ALTER TABLE "t_users_blocks" ADD FOREIGN KEY ("user_account_id_given") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;
ALTER TABLE "t_users_blocks" ADD FOREIGN KEY ("user_account_id_received") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;

ALTER TABLE "t_users_likes" ADD FOREIGN KEY ("user_account_id_given") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;
ALTER TABLE "t_users_likes" ADD FOREIGN KEY ("user_account_id_received") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;

ALTER TABLE "t_users_photos" ADD FOREIGN KEY ("user_account_id") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;

ALTER TABLE "t_users_bios" ADD FOREIGN KEY ("user_account_id") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;

ALTER TABLE "t_users_genders" ADD FOREIGN KEY ("user_account_id") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;
ALTER TABLE "t_users_genders" ADD FOREIGN KEY ("gender_id") REFERENCES "t_genders" ("id_gender") ON DELETE CASCADE;

ALTER TABLE "t_users_interest_in_genders" ADD FOREIGN KEY ("user_account_id") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;
ALTER TABLE "t_users_interest_in_genders" ADD FOREIGN KEY ("gender_id") REFERENCES "t_genders" ("id_gender") ON DELETE CASCADE;

ALTER TABLE "t_users_interest_in_relationships" ADD FOREIGN KEY ("user_account_id") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;
ALTER TABLE "t_users_interest_in_relationships" ADD FOREIGN KEY ("relationship_id") REFERENCES "t_relationships" ("id_relationship") ON DELETE CASCADE;

ALTER TABLE "t_users_sizes" ADD FOREIGN KEY ("user_account_id") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;
ALTER TABLE "t_users_sizes" ADD FOREIGN KEY ("size_id") REFERENCES "t_sizes" ("id_size") ON DELETE CASCADE;

ALTER TABLE "t_users_shapes" ADD FOREIGN KEY ("user_account_id") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;
ALTER TABLE "t_users_shapes" ADD FOREIGN KEY ("shape_id") REFERENCES "t_shapes" ("id_shape") ON DELETE CASCADE;

ALTER TABLE "t_users_eyes" ADD FOREIGN KEY ("user_account_id") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;
ALTER TABLE "t_users_eyes" ADD FOREIGN KEY ("eye_id") REFERENCES "t_eyes" ("id_eye") ON DELETE CASCADE;

ALTER TABLE "t_users_hairs_cuts" ADD FOREIGN KEY ("user_account_id") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;
ALTER TABLE "t_users_hairs_cuts" ADD FOREIGN KEY ("hair_cut_id") REFERENCES "t_hairs_cuts" ("id_hair_cut") ON DELETE CASCADE;

ALTER TABLE "t_users_hairs_styles" ADD FOREIGN KEY ("user_account_id") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;
ALTER TABLE "t_users_hairs_styles" ADD FOREIGN KEY ("hair_style_id") REFERENCES "t_hairs_styles" ("id_hair_style") ON DELETE CASCADE;

ALTER TABLE "t_users_hairs_colors" ADD FOREIGN KEY ("user_account_id") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;
ALTER TABLE "t_users_hairs_colors" ADD FOREIGN KEY ("hair_color_id") REFERENCES "t_hairs_colors" ("id_hair_color") ON DELETE CASCADE;

ALTER TABLE "t_users_origins" ADD FOREIGN KEY ("user_account_id") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;
ALTER TABLE "t_users_origins" ADD FOREIGN KEY ("origin_id") REFERENCES "t_origins" ("id_origin") ON DELETE CASCADE;

ALTER TABLE "t_users_astrosigns" ADD FOREIGN KEY ("user_account_id") REFERENCES "t_users_accounts" ("id_user_account") ON DELETE CASCADE;
ALTER TABLE "t_users_astrosigns" ADD FOREIGN KEY ("astrosign_id") REFERENCES "t_astrosigns" ("id_astrosign") ON DELETE CASCADE;
